use mpd::{Client, Song, State, Status};
use notify_rust::{Notification, Timeout};
use sedregex::find_and_replace;
use std::fmt;

// struct and impl needed for formatting player state as a String
struct PlayState {
    sta: State,
}

impl fmt::Display for PlayState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.sta)
    }
}

// Borrowed from JakeStanger, I don't usually do this, but I thought I was going to go insane
// If you come across this and you're mad, I sincerely apologize and will remove/replace this
fn format_time(time: i64) -> String {
    let seconds = (time as f64 % 60.0).round();
    let minutes = ((time as f64 % 3600.0) / 60.0).round();

    format!("{:0>2}:{:0>2}", minutes, seconds)
}

// (format, title, artist, album, date, genre) -> (String, String, String, String, String, Sting)
fn info(c: &mut Client) -> (String, String, String, String, String, String) {
    let na = String::from("N/A");
    let song: Song = c.currentsong().unwrap().unwrap();
    let fil = song.file;
    let format = find_and_replace(&fil, &["s/.*\\.//"]).unwrap();
    let tit = song.title.as_ref().unwrap();
    let art = song.tags.get("Artist").unwrap_or(&na);
    let alb = song.tags.get("Album").unwrap_or(&na);
    let dat = song.tags.get("Date").unwrap_or(&na);
    let gen = song.tags.get("Genre").unwrap_or(&na);
    (
        format.to_string(),
        tit.to_string(),
        art.to_string(),
        alb.to_string(),
        dat.to_string(),
        gen.to_string(),
    )
}

// (elapsed, duration, bitrate) -> (String, String, String)
fn info_extended(status: &mut Status) -> (String, String, String) {
    let elap = status.elapsed.unwrap().num_seconds();
    let elapsed = format_time(elap);
    let dur = status.duration.unwrap().num_seconds();
    let duration = format_time(dur);
    let bitrate = status.bitrate.unwrap().to_string();
    let bitrate = bitrate + &" kbps".to_string(); // bitrate kbps
    (elapsed, duration, bitrate)
}

fn main() {
    let mut c = Client::connect("127.0.0.1:6600").unwrap(); // Connect to MPD
    let mut status: Status = c.status().unwrap();
    let (format, tit, art, alb, dat, gen) = info(&mut c);
    let (elapsed, duration, bitrate) = info_extended(&mut status);
    let stat = status.state;
    let state = PlayState { sta: stat };
    let info = format + &" @ " + &bitrate; // "format @ bitrate kbps"
    // elapsed/duration [state]
    // title [format @ bitrate kbps]
    // album [date]
    // artist
    // genre
    // ^^ Output of `msg`
    let msg = elapsed
        + &"/".to_string()
        + &duration
        + &" [".to_string()
        + &state.to_string()
        + &"]\n".to_string()
        + &tit
        + &" [".to_string()
        + &info
        + &"]\n".to_string()
        + &alb
        + &" [".to_string()
        + &dat
        + &"]\n".to_string()
        + &art
        + &"\n".to_string()
        + &gen;
    Notification::new()
        .summary(&msg)
        .icon("/tmp/cover.png") // Cover art should be wrote to `/tmp/cover.png`
        .timeout(Timeout::Milliseconds(6000)) // Notification closes in 6s
        .show()
        .unwrap();
}
